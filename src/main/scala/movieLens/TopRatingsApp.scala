package movieLens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac006 on 2017/5/15.
  */
object TopRatingsApp extends App{
  val conf=new SparkConf().setAppName("TopRatings").setMaster("local[*]")
  val sc=new SparkContext(conf)

  val ratings : RDD[(Int,Double)]=sc.textFile("/Users/mac006/Downloads/ml-20m/ratings.csv")
    .map(str=>str.split(","))
      .filter(strs=>strs(1)!="movieId")  //去掉第一行:userId,movieId,rating,timestamp
    .map(strs=>
    //{println(strs.mkString(","))
      (strs(1).toInt,strs(2).toDouble)
    //}
  )
  //  取五個列出來
  ratings.take(5).foreach(println)
  //  隨機抽樣,抽五個曲後不放回
  ratings.takeSample(false,5).foreach(println)


  //  相同的movieId的Rating加總
  val totalRatingByMovieId = ratings.reduceByKey((acc,curr)=>acc+curr)
  //  隨機抽樣,抽五個曲後不放回
  totalRatingByMovieId.takeSample(false,5).foreach(println)

  //  每部電影的平均評分,原本為RDD[(Int,(Double,Int))],但是要相除時會變成RDD[(Int,Double)]
  val averageRatingByMovieId : RDD[(Int,Double)]= ratings.mapValues(v=> v->1)
    .reduceByKey((acc,curr)=>{(acc._1+curr._1)->(acc._2+curr._2)})
    .mapValues(kv=> kv._1 / kv._2.toDouble)
  averageRatingByMovieId.takeSample(false,5).foreach(println)

  val top10=averageRatingByMovieId.sortBy(_._2,false).take(10)
  top10.foreach(println)


  val movies =sc.textFile("/Users/mac006/Downloads/ml-20m/movies.csv")
    .map(str=>str.split(","))   // 用逗號做分割
     .filter(strs=>strs(0)!="movieId")
    .map(strs=>(strs(0).toInt->strs(1)))
    movies.take(10).foreach(println)

  val joined:RDD[(Int,(Double,String))]=averageRatingByMovieId.join(movies)
  //joined.takeSample(false,5).foreach(println)

  // 找出top5的影片名稱
  val top5=joined.sortBy(_._2,false).take(5)

  //  joined.map(v=>v._1+","+v._2._2+","+v._2._1).saveAsTextFile("result")
  joined.saveAsTextFile("result")
  sc.textFile("result").map(str=>str.split(","))
    .map(strs=>strs(0).toInt->strs(1->strs(2).toDouble))

}

/**
  * Created by mac006 on 2017/5/8.
  */

import org.apache.spark.{SparkConf, SparkContext}


object FilterDemoApp extends App{
  val conf=new SparkConf().setAppName("FilterDemo").setMaster("local[*]")
  val sc=new SparkContext(conf)

  //odds
  sc.parallelize(1 to 100).filter(i=>i%2==1)
  .foreach(v=>println(v))

  //firstLetterIsM
  sc.textFile("./names.txt")
    .filter(name=>name.startsWith ("M"))
    //or:
    //.filter(name=>name.head=='M')
    .foreach(println)
}
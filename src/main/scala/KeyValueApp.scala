import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac006 on 2017/5/8.
  */
object KeyValueApp extends App{

  val conf= new SparkConf().setAppName("KeyValue")   .setMaster("local[*]")

  val sc= new SparkContext(conf)

  val kvRdd=sc.parallelize(1 to 100).map(v=>{
    if (v%2==0) "even" -> v else "odd" ->v
  })

  //1 to 10 odd or even
  kvRdd.take(10).foreach(println)

  //odd=>even,even=>odd
  kvRdd.mapValues(_+1).take(10).foreach(println)

  // group odd and even
  kvRdd.groupByKey().foreach(println)

  //sum odd and even
  kvRdd.groupByKey.mapValues(v=>v.sum).foreach(println)

  //or:
  kvRdd.reduceByKey((acc,curr)=>acc+curr).foreach(println)

}

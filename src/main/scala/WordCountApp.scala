import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac006 on 2017/5/8.
  */
object WordCountApp extends App{
  val conf=new SparkConf().setAppName("WordCount").setMaster("local[*]")
  val sc=new SparkContext(conf)

  val lines=sc.textFile("/Users/mac006/Downloads/spark-doc.txt")
  //lines.flatMap(str=>str.split(" ").toList.take(10)).foreach(println)
  val words=lines.flatMap(str=>str.split(" "))

   // words.groupBy(str=>str).mapValues(_.size).take(10).foreach(println) 以前學的
  words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr)
     //.take(10)
  .foreach(println)

  // find spark
  //words.map(word=>word->1)reduceByKey((acc,curr)=>acc+curr).foreach(println)
  //.filter(_,_==spark)
 // readLine()
}
